# Apple II DOS

[[_TOC_]]

## Could you write a Disk Operating System in 7 weeks?

**In June 1977 Apple Computer shipped their first mass-market computer: the
Apple II.**

---

![](assets/apple-ii-dos-source-code-apple-ii.jpg){width=25%}

*Apple II, designed by Steve Wozniak in 1977.*

---

![](assets/apple-ii-dos-source-code-price-list-oct-1977.jpg){width=25%}

*The MSRP list for Apple II machines. Prices ranged from $798 for a standalone
board with 4KB RAM to $2,770 for a fully-assembled computer with 48KB RAM.*

---

Unlike the Apple I, the Apple II was fully assembled and ready to use with any
display monitor. The version with 4K of memory cost \$1298. It had colour,
graphics, sound, expansion slots, game paddles, and a built-in BASIC
programming language.

What it didn’t have was a disk drive. Programs and data had to be saved and
loaded from cassette tape recorders, which were slow and unreliable. The
problem was that disks – even floppy disks – needed both expensive hardware
controllers and complex software.

Steve Wozniak solved the first problem. He designed an incredibly clever
floppy disk controller using only 8 integrated circuits, by doing in
programmed logic what other controllers did with hardware. With some
rudimentary software written by Woz and Randy Wigginton, it was demonstrated
at the Consumer Electronics Show in January 1978.

But where were they going to get the higher-level software to organize and
access programs and data on the disk? Apple only had about 15 employees, and
none of them had both the skills and the time to work on it.

---

![](assets/apple-ii-dos-source-code-paul-laughton-1980.jpg){width=25%}

*Paul Laughton, the programmer who led the Apple II DOS project, in his home
office in 1980.*

---

The magician who pulled that rabbit out of the hat was Paul Laughton, a
contract programmer for Shepardson Microsystems, which was located in the same
Cupertino office park as Apple.

On April 10, 1978 Bob Shepardson and Steve Jobs signed a \$13,000 one-page
contract for a file manager, a BASIC interface, and utilities. It specified
that “Delivery will be May 15″, which was incredibly aggressive. But,
amazingly, “Apple II DOS version 3.1″ was released in June 1978.

There are seven files in this release that may be downloaded by clicking the
hyperlinked filename:

- [AppleDOS2June1978.pdf: Scanned lineprinter listing from June 2, 1978](assets/AppleDOS2June1978.pdf)

- [AppleDOS6Oct1978.pdf: Scanned lineprinter listing from October 6, 1978](assets/AppleDOS6Oct1978.pdf)

- [AppleDOS6Oct1978_retyped.docx: Retyped source code of the October 6th version](assets/Apple_DOS_6Oct1978_retyped.docx) (This has not yet been assembled, and there may be some typographical errors.)

- [AppleDOSRW_30May1978.txt: The source code of the low-level read/write routines by Steve Wozniak and Randy Wigginton.](assets/Apple_DOS_RW_30May1978.txt)

- [AppleDOStech_docs.pdf: Various technical specifications and designs relating to the Apple II disk drive](assets/AppleDOStech_docs.pdf)

- [AppleDOScontracts.pdf: Various contracts and addenda between Apple and Shepardson Microsystems](assets/AppleDOScontracts.pdf)

- [Apple_DOS_meeting_5Oct1978.pdf: Minutes of a meeting between Apple and Shepardson Microsystem about bugs and enhancements.](assets/Apple_DOS_meeting_5Oct1978.pdf) (Unfortunately we don’t have the list that is referred to.)

Scott LaBombard, a dedicated vintage computer collector/restorer in New York,
has done an amazing job researching and reconstructing Apple II DOS source
code.

[Apple II DOS source code](src)

The “shepdos” directory in that archive represents his work on the scanned
listings we got from Paul Laughton, and the “appledos31″ directory contains
reconstructed source code that matches the production DOS 3.1 binaries
released by Apple.

## More on the story behind Apple II DOS

### Finding the right programmer

Paul Laughton had taken computer programming classes at Virginia Tech, and had
worked as a programmer for John Hopkins University and IBM. He was experienced
with the design of large mainframe timesharing systems like IBM’s CALL/360,
and he knew about compilers and operating systems.

---

![](assets/apple-ii-dos-source-code-homebrew-1024x617.jpg){width=25%}

---

But Paul was also following the development of small personal computers based
on the new microprocessors. He sometimes, with his programmer wife Kathleen
O’Brien, attended the Homebrew Computer Club meetings at Stanford.

In the meantime, Bob Shepardson had a small contract programming group in
Cupertino that was writing assemblers for those new microprocessor-based
systems. As Paul says [1] ”Apple was a few doors down. A very, very small
company, maybe 10 or 15 people. They were working on a new computer called
“Apple Annie”, and they wanted a BASIC for that. …Bob was busy doing other
things, so he put a small ad in the paper and I responded.” Paul, who had
worked on BASIC for mainframe computers, was hired for the job.

But while Paul was working on the BASIC for Apple Annie (a never-released
successor to the Apple II), something happened. “Steve Wozniak came into the
office showing off his new disk drive”. One thing led to another, and Paul
agreed to write the Disk Operating System for the new Apple II disk drive.

---

![](assets/apple-ii-dos-source-code-cassette-label-1024x645.jpg){width=25%}

*A cassette containing the source of Apple DOS.*

---

The contract was signed in April 1978, and delivery was for that June. “I knew
what needed to be done, and I knew how to do it. I was confident that I could
do it pretty quickly.” All he had to start with was the low-level “read/write
track/sector” routine that had been written by Woz and Wigginton.

Laughton indeed “knew how to do it”. The first delivery was in early June
1978, within a week of the deadline specified in the one-page contract.

## The Woz Disk Controller

The hardware Paul had to work with was a masterful design. “I was familiar
with the other disk controllers, and when I saw Woz’s design I was just blown
away. I mean, it was so simple, and so elegant. And the fact that he was doing
it in software — a lot of the work that was done by those massive amounts of
chips was done in a few lines of software. It was just pure genius.”

---

![](assets/apple-ii-dos-source-code-disk-controller-1024x684.jpg){width=25%}

*Disk Controller*

---

![](assets/apple-ii-dos-source-code-woz-jobs-1978-1024x796.jpg){width=25%}

*Steve Wozniak and Steve Jobs, cir. 1978.*

---

## Writing Apple II DOS

When it came time to write DOS, there was a problem: the Apple II itself was
not capable of assembling programs for its own MOS Technology 6502
microprocessor. The program had to be assembled on other machines.

“DOS was written on punch cards. I would actually hand-write the code on
80-column punch card sheets. A guy at Shepardson named Mike Peters would take
those sheets and punch the cards. The punch cards would then be read into a
National Semiconductor IMP-16 and assembled, and a paper tape produced. The
paper tape was read into the Apple II by a plug-in card made by Wozniak, and I
would proceed to debug it. As the project got further along and the code was
all written, and it was debugging and updating, I would mark up a listing and
give it to Mike Peters who would then change whatever was necessary and
deliver me a paper tape and I’d start again.”

---

![](assets/apple-ii-dos-source-code-marked-up-listing.jpg){width=25%}

*A marked-up page of the source code for Apple II DOS, written in Assembly for
the 6502.*

---

## In his own words

### Oral history of Paul Laughton

[![](https://img.youtube.com/vi/EeEGJg6vXCg/hqdefault.jpg)](https://youtu.be/EeEGJg6vXCg)

*Interviewed by Bruce Damer, with Len Shustek, on 2013-08-14 in Mountain View,
California, X6895.2014.*

## What it was, and what it wasn't

Writing Apple DOS in seven weeks was an amazing accomplishment. But to be
fair, it wasn’t an “operating system” as we use the term now. “When I began
the project, what I had in mind was a file management system. This creates a
structure whereby you can open, read, write and close files. …In order to use
it you had to write some code… there were no commands; you had to do it
yourself. …A true operating system has a lot of other components to it,
interfacing to other pieces of hardware, USB, the networks, screens,
keyboards. A true operating system is much, much, more than what I did for
Apple.”

Paul initially provided a programmer’s interface to DOS. But Apple wanted a
simpler way to access the file system from BASIC programs. Somewhat
reluctantly, Paul did that too, after the first June delivery of the code. “I
ended up writing the code to interface BASIC to that file management
system. One of the things that always disturbed me was I had a very clean
interface to the file management system. There were control blocks and places
you could call to do things. It never really got published. Instead, what you
saw was the BASIC interface.” That interface was released by Apple in October
of 1978, and then anyone who could write in BASIC could use the disk.

But APIs (Application Programming Interfaces) want to be free. “I have
encountered people in my life since then who disassembled the code and figured
it out. They ended up writing interfaces to it the way that I thought it
should be done. These were mainly game developers.”

## What was next for Paul

---

![](assets/apple-ii-dos-source-code-paul-laughton-2013.jpg){width=25%}

*Paul Laughton examining the Apple II DOS source code listing in August 2013*

---

After the Apple II DOS contract was completed, Paul Laughton continued to work
for Shepardson Microsystems on contracts for DOS and BASIC for Atari, and on a
C compiler for Cromemco. When Shepardson closed the business, Paul become an
Atari employee for a few years, and later joined Fox Video Games. In the
mid-80′s he participated in a startup for which he created a complete digital
voicemail system. Following that, he worked for Logitech and other companies
on software for digital cameras.

Paul retired in 2000, but remains active in engineering. He has written a
popular Android-based BASIC, and is working on software-defined radios for
amateur radio “hams”. He is also training to become a docent at the Computer
History Museum.

---

[Oral history of Paul Laughton, August 14, 2013, CHM X6895.2014. All otherwise unattributed quotes are from that interview.](assets/102746742-05-01-acc.pdf)
